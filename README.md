## Easy html form building ##
### Secure data collection forms with powerful features ###
Finding good forms? We build web forms, online surveys, questionnaires, and polls, in addition to easy ecommerce

**Our objectives:**

* Multiple language
* Form building
* Optimization
* A/B testing
* Custom templates
* Payment integration

### The most powerful, flexible, and easiest form builder in existence ###
Take a look at our [html form](https://formtitan.com) and see why it’s fast becoming the most popular form builder around

Happy html form!